=================================================================
A Custom 3D Printed Adjustable Tripod Adapter for Your Smartphone
=================================================================

`A Custom 3D Printed Adjustable Tripod Adapter for Your Smartphone`__ by
Lab290A_ is licensed under `CC BY-NC-SA 4.0`_ |cc|_ |by|_ |nc|_ |sa|_

The project and gcode files are mine and licences under the same
licence as well as the files for the threaded insert adapter.

The thread inserts I used are from Xiesk [#]_.

.. |cc| image:: https://mirrors.creativecommons.org/presskit/icons/cc.svg
   :height: 20px
   :width: 20px
.. _cc: https://creativecommons.org/licenses/by-nc-sa/4.0/
.. |by| image:: https://mirrors.creativecommons.org/presskit/icons/by.svg
   :height: 20px
   :width: 20px
.. _by: https://creativecommons.org/licenses/by-nc-sa/4.0/
.. |nc| image:: https://mirrors.creativecommons.org/presskit/icons/nc.svg
   :height: 20px
   :width: 20px
.. _nc: https://creativecommons.org/licenses/by-nc-sa/4.0/
.. |sa| image:: https://mirrors.creativecommons.org/presskit/icons/sa.svg
   :height: 20px
   :width: 20px
.. _sa: https://creativecommons.org/licenses/by-nc-sa/4.0/

__ Instructables_
.. _Instructables: https://www.instructables.com/A-Custom-3D-Printed-Adjustable-Tripod-Adapter-for-/
.. _Lab290A: https://www.instructables.com/member/Lab290A/
.. _CC BY-NC-SA 4.0: https://creativecommons.org/licenses/by-nc-sa/4.0/
.. [#] https://duckduckgo.com/?q=XIESK+Pack+of+300+Thread+Inserts+M2+M2.5+M3+M4+M5+M6+Knurled+Nuts+Brass+Internal+Thread+for+Plastic+Parts+or+3D+Pressure+Parts+by+Ultrasonic+Warm+Embedding&t=ffab&ia=web
